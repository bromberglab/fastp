#!/bin/bash
#
# This is a rather minimal example Argbash potential
# Example taken from http://argbash.readthedocs.io/en/stable/example.html
#
# ARG_OPTIONAL_SINGLE([in1],[i])
# ARG_OPTIONAL_SINGLE([in2],[I])
# ARG_OPTIONAL_SINGLE([out1],[o])
# ARG_OPTIONAL_SINGLE([out2],[O])
# ARG_LEFTOVERS([])
# ARGBASH_GO()
# needed because of Argbash --> m4_ignore([
### START OF CODE GENERATED BY Argbash v2.8.1 one line above ###
# Argbash is a bash code generator used to get arguments parsing right.
# Argbash is FREE SOFTWARE, see https://argbash.io for more info
# Generated online by https://argbash.io/generate


die()
{
	local _ret=$2
	test -n "$_ret" || _ret=1
	test "$_PRINT_HELP" = yes && print_help >&2
	echo "$1" >&2
	exit ${_ret}
}


begins_with_short_option()
{
	local first_option all_short_options='iIoO'
	first_option="${1:0:1}"
	test "$all_short_options" = "${all_short_options/$first_option/}" && return 1 || return 0
}

# THE DEFAULTS INITIALIZATION - POSITIONALS
_positionals=()
_arg_leftovers=()
# THE DEFAULTS INITIALIZATION - OPTIONALS
_arg_in1=
_arg_in2=
_arg_out1=
_arg_out2=


print_help()
{
	printf 'Usage: %s [-i|--in1 <arg>] [-I|--in2 <arg>] [-o|--out1 <arg>] [-O|--out2 <arg>] ... \n' "$0"
}


parse_commandline()
{
	_positionals_count=0
	while test $# -gt 0
	do
		_key="$1"
		case "$_key" in
			-i|--in1)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_in1="$2"
				shift
				;;
			--in1=*)
				_arg_in1="${_key##--in1=}"
				;;
			-i*)
				_arg_in1="${_key##-i}"
				;;
			-I|--in2)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_in2="$2"
				shift
				;;
			--in2=*)
				_arg_in2="${_key##--in2=}"
				;;
			-I*)
				_arg_in2="${_key##-I}"
				;;
			-o|--out1)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_out1="$2"
				shift
				;;
			--out1=*)
				_arg_out1="${_key##--out1=}"
				;;
			-o*)
				_arg_out1="${_key##-o}"
				;;
			-O|--out2)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_out2="$2"
				shift
				;;
			--out2=*)
				_arg_out2="${_key##--out2=}"
				;;
			-O*)
				_arg_out2="${_key##-O}"
				;;
			*)
				_last_positional="$1"
				_positionals+=("$_last_positional")
				_positionals_count=$((_positionals_count + 1))
				;;
		esac
		shift
	done
}


assign_positional_args()
{
	local _positional_name _shift_for=$1
	_positional_names=""
	_our_args=$((${#_positionals[@]} - 0))
	for ((ii = 0; ii < _our_args; ii++))
	do
		_positional_names="$_positional_names _arg_leftovers[$((ii + 0))]"
	done

	shift "$_shift_for"
	for _positional_name in ${_positional_names}
	do
		test $# -gt 0 || break
		eval "$_positional_name=\${1}" || die "Error during argument parsing, possibly an Argbash bug." 1
		shift
	done
}

parse_commandline "$@"
assign_positional_args 1 "${_positionals[@]}"

# OTHER STUFF GENERATED BY Argbash

### END OF CODE GENERATED BY Argbash (sortof) ### ])
# [ <-- needed because of Argbash

main() {
	_args=""
	[ ! -z "$_arg_in1" ] && _args="$_args--in1 $_arg_in1 "
	[ ! -z "$_arg_in2" ] && _args="$_args--in2 $_arg_in2 "
	[ ! -z "$_arg_in1" ] && _args="$_args--out1 $_arg_out1 "
	[ ! -z "$_arg_in2" ] && _args="$_args--out2 $_arg_out2 "
	fastp ${_args}$(printf "%s " "${_arg_leftovers[@]}") && return 0

	# reached in case of failure
	echo "Cleaning up ..."
	[ -d "$(dirname "$_arg_out1")" ] && rm -rf "$(dirname "$_arg_out1")"
	[ -d "$(dirname "$_arg_out2")" ] && rm -rf "$(dirname "$_arg_out2")"
	return 0
}
main

# ] <-- needed because of Argbash