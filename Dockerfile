FROM ubuntu:18.04 as base

LABEL maintainer=mmiller@bromberglab.org \
      description="An ultra-fast all-in-one FASTQ preprocessor (QC/adapters/trimming/filtering/splitting/merging...)"

# bio-node
LABEL bio-node=v1.0 \
      input_1="fastq_file|fastq_gz,--in1,required,filename" \
      input_2="fastq_file|fastq_gz,--in2,optional,filename" \
      output_1="fastq_file,--out1,fastq_qc_1.fastq" \
      output_2="fastq_file,--out2,fastq_qc_2.fastq" \
      output_3="fastp_json_report,--json,report.json" \
      output_4="fastp_html_report,--html,report.html" \
      app_entrypoint="/app/fastp_bionode.sh" \
      parameters="--qualified_quality_phred 20 --length_required 40 --compression 4 --thread 2 --report_title='mifaser.quality-control'"

FROM base as builder

# setup app
COPY . /app

# setup system
RUN apt-get update -y && apt-get install -y \
    build-essential \
    #libnss-sss \
    curl \
    wget \
    zlib1g-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /tmp
RUN LATEST=$(basename $(curl -Ls -o /dev/null -w %{url_effective} https://github.com/OpenGene/fastp/releases/latest/download/) | cut -c 2-) && \
    echo ${LATEST} > /app/version && \
    wget -q https://github.com/OpenGene/fastp/archive/v${LATEST}.tar.gz && \
    tar xzf v${LATEST}.tar.gz && \
    cd fastp-${LATEST} && \
    mkdir -p /install/bin && PREFIX=/install make && PREFIX=/install make install

FROM base

COPY --from=builder /install /usr/local
COPY --from=builder /app /app

# set environment variables
WORKDIR /input

ENTRYPOINT ["fastp"]

# set project CMD
# CMD []
